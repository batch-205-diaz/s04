// index.js

class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}

	login(){

      console.log(`${this.email} has logged in`);
      return this;
   }

  	logout(){
      console.log(`${this.email} has logged out`);
      return this;
   }

   listGrades(){
      this.grades.forEach(grade => {
         console.log(grade);
      })
      return this;
   }

   computeAve(){
      let sum = this.grades.reduce((accumulator,num) => accumulator += num)
      this.average = Math.round(sum/4);
      return this;
   }

   willPass() {
      this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
      return this;
   }

   willPassWithHonors() {

      this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
      return this;
   }

}
   
/*
   class SECTION
   -constructor(name) - every instance of Section class will be instantiated with an empty array for our students.
   -addStudents method will allow us to add instances of the Student class as items for our students property.
   -this.students.push - S student instance/object will b instantiated with the name,email,grades, and push into our students property
   - countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors
      - need to add 1 to a temp variable to hold counter (no. of honor studs)
*/

class Section {

   constructor(name){

        this.name = name;
        this.students = [];
        this.sectionAve = undefined;
        this.passedStudents = undefined;
        this.honorStudents = undefined;

   }

   addStudent(name,email,grades){

      this.students.push(new Student(name,email,grades));
      return this;
   }

   countPassedStudents(){
      let count = 0;
      this.students.forEach(student => {

         student.willPass();
         if(student.isPassed){
            count++
         };
      })

      this.passedStudents = count;
      return this;
   }

   countHonorStudents(){
      let count = 0;
      this.students.forEach(student => {
         // console.log(student);
         // console.log(student.willPassWithHonors().isPassedWithHonors);

         student.willPassWithHonors();
         if(student.isPassedWithHonors){
            count++
         };
      })

      this.honorStudents = count;
      return this;
   }

   getNumberOfStudents(){
      return this.students.length
   }

   computeSectionAve(){

      //make array for all average
      let i = 0;
      let averageArray = [];
      while(i < this.students.length){

         //invoke compute average
         this.students[i].computeAve();

         //populate array
         averageArray.push(this.students[i].computeAve().average);
         i++
      }

      //compute section average
      let sum = 0;
      averageArray.forEach(grade => {
         sum += grade;
      })

      //update property and return this
      this.sectionAve = sum/this.students.length
      return this
   }

}

let section1A = new Section("Section1A");
console.log(section1A);

//3 arguments that are needed to instantiate our Student
section1A.addStudent("Joy","joy@gmail.com",[89,91,92,88]);
section1A.addStudent("Jeff","jeff@gmail.com",[81,80,82,78]);
section1A.addStudent("John","john@gmail.com",[91,90,92,96]);
section1A.addStudent("Jack","jack@gmail.com",[95,92,92,93]);

//check details of John from section1A
console.log(section1A.students[2]);

//check the average of John from section1A?
console.log("average: ",section1A.students[2].computeAve().average);

//check if Jeff from section1A passed?
console.log("Jeff's Average: ",section1A.students[1].computeAve().average)
console.log("Did Jeff pass? ",section1A.students[1].willPass().isPassed)


/* ========== ACTIVITY ========== */

section1A.addStudent("Alex","alex@mail.com",[84,85,85,86]);